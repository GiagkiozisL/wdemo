package com.wolt.wdemo

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wolt.wdemo.data.model.NetworkResult
import com.wolt.wdemo.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val tag = "[MainActivity]"
    private val viewModel by viewModels<MainViewModel>()
    private lateinit var binding: ActivityMainBinding
    private lateinit var restaurantsAdapter: RestaurantsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAdapter()
        startPolling()
    }

    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(this)
        val onFavouriteClicked: (oid: String, isChecked: Boolean) -> Unit = { oid, isCheched ->
            viewModel.favouriteClicked(oid, isCheched)
        }

        restaurantsAdapter = RestaurantsAdapter(onFavouriteClicked)
        binding.mainRecyclerView.adapter = restaurantsAdapter
        binding.mainRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            addItemDecoration(DividerItemDecoration(context, (layoutManager as LinearLayoutManager).orientation))
        }
    }

    private fun startPolling() {
        lifecycleScope.launchWhenStarted {
            viewModel.poll
                .collect { data ->
                    println("$tag data: $data")
                    setupDataObserver(data.latitude, data.longitude)
                }
        }
    }

    private fun setupDataObserver(lat: Double, lng: Double) {
        println("$tag asking for new spots..")
        /**
         * ## developer message ##
         * usage of observeOnce instead of observe
         * removes observer after first result
         * */
        viewModel.results.observeOnce(this, Observer { netResult ->
            when (netResult) {
                is NetworkResult.Error -> {
                    binding.mainProgressBar.visibility = View.GONE
                    binding.mainTextView.text = netResult.message
                    println("$tag fetching results failed with error ${netResult.message}")
                }

                is NetworkResult.Loading -> {
                    binding.mainProgressBar.visibility = View.VISIBLE
                    println("$tag fetching results...")
                }

                is NetworkResult.Success -> {
                    binding.mainProgressBar.visibility = View.GONE
                    netResult.data?.let {
                        restaurantsAdapter.refreshData(it)
                    }
                    println("$tag results has been fetched successfully")
                }
            }
        })
        viewModel.fetchRestaurantsByLocation(lat, lng)
    }
}