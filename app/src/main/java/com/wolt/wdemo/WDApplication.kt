package com.wolt.wdemo

import android.app.Application
import com.wolt.wdemo.di.LocationData
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject


@HiltAndroidApp
class WDApplication: Application() {

    @Inject
    lateinit var locationData: LocationData

    override fun onCreate() {
        super.onCreate()

        /**
         *
         * ## developer message ##
         *
         *  Q. Should the current location continue changing if the app is put to background?
         *
         *  A1. We could place the location-rotation procedure in a service which can run independently
         *  A2. We could place the location-rotation procedure in a Global CoroutineScope which can
         *      be used to top-level context but with danger of memory leak. Of course a task like this
         *      is not that much heavy, also i'd rather to not choose that option.
         *  A3. I will set a timestamp (here) when the app is opened and make the calculation of estimated time in my view model.
         *      The timestamp variable it will be stored in a singleton class.
         *
         *  I ve chosen the 3rd option for this particular task, because i believe its the simplest & cleanest solution.
         *  In case we had more complicated UI with navigation between activities and others then my approach will be different.
         *
         * **/

        locationData.appOpenedMillis = System.currentTimeMillis()
        println("application app opend: ${locationData.appOpenedMillis}")
    }
}