package com.wolt.wdemo.di

import com.wolt.wdemo.LatLng
import javax.inject.Inject
import javax.inject.Singleton

private const val tag = "[LocationData]"

@Singleton
class LocationData @Inject constructor(
) {

    var appOpenedMillis: Long = 0

    // hardcoded coordinates as input
    val coordinates = arrayOf(
        LatLng(60.170187, 24.930599),
        LatLng(60.169418, 24.931618),
        LatLng(60.169818, 24.932906),
        LatLng(60.170005, 24.935105),
        LatLng(60.169108, 24.936210),
        LatLng(60.168355, 24.934869),
        LatLng(60.167560, 24.932562),
        LatLng(60.168254, 24.931532),
        LatLng(60.169012, 24.930341),
        LatLng(60.170085, 24.929569))

    /**
     *
     * calculating index of the list
     * from the difference between appOpenedTimestamp & nowTimestamp
     *
     * i'd rather to avoid the creation of a background coroutine
     * for such minor need
     *
     * */

    fun getCoordinates() : LatLng {
        val nowMillis = System.currentTimeMillis()
        val diffMillis = nowMillis - appOpenedMillis
        val diffSecs = diffMillis / 1000
        val nTimes = (diffSecs / coordinates.size).toInt()
        val div = nTimes % coordinates.size
        println("$tag diffSecs:$diffSecs, nTimes:$nTimes, div:$div nowmillis:$nowMillis appopened:$appOpenedMillis")
        return coordinates[div]
    }
}