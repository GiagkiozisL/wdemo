package com.wolt.wdemo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wolt.wdemo.data.MainRepository
import com.wolt.wdemo.data.model.NetworkResult
import com.wolt.wdemo.data.model.Result
import com.wolt.wdemo.di.LocationData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val tag = "[MainViewModel]"
private const val nItems = 15
private const val pollInterval: Long = 10000 // milliseconds

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val locationData: LocationData
) : ViewModel() {

    private val _results = MutableLiveData<NetworkResult<List<Result>>>()
    val results: LiveData<NetworkResult<List<Result>>>
        get() = _results

    private val favouriteRestIds: MutableSet<String> = mutableSetOf()


    val poll: Flow<LatLng> = flow {
        // Moved into the flow builder block.
        while (true) {
            delay(pollInterval)
            emit(locationData.getCoordinates())
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(0, Long.MAX_VALUE),
        initialValue = locationData.coordinates[0]
    )

    fun fetchRestaurantsByLocation(lat: Double, lng: Double) {
        println("$tag latitude: $lat")
        _results.value = NetworkResult.Loading()

        viewModelScope.launch {
            mainRepository.restaurants(lat, lng)
                .catch { exception -> println("$tag fetching exception: ${exception.message}") }
                .collect { values ->
                    when (values) {
                        is NetworkResult.Success -> {
                            // get n first items
                            val desiredResults = values.data?.results?.take(nItems)!!

                            /**
                             * ## developer message ##
                             * i preferred to filter the data of 15 items
                             * instead of whole result (50 items)
                             * to improve performance of iteration
                             **/
                            val list = desiredResults.toMutableList()
                            list.mapInPlace { element ->
                                val exists = favouriteRestIds.contains(element.activeMenu?.oid)
                                element.favourite = exists
                                element
                            }

                            // populate data
                            _results.value = NetworkResult.Success(desiredResults)
                            println("$tag total restaurants: ${values.data.results.count()}")
                        }

                        is NetworkResult.Error -> {
                            // show error
                            val errorMessage = values.message ?: "Unknown Error!!"
                            _results.value = NetworkResult.Error(errorMessage)
                        }

                        is NetworkResult.Loading -> {
                            // show loading
                            _results.value = NetworkResult.Loading()
                        }
                    }
                }
        }
    }

    fun favouriteClicked(oid: String, isChecked: Boolean) {
        // add/ remove from cached list
        if (isChecked) favouriteRestIds.add(oid) else favouriteRestIds.remove(oid)
        println("$tag read my fav set $favouriteRestIds")
    }
}

class LatLng(val latitude: Double, val longitude: Double)