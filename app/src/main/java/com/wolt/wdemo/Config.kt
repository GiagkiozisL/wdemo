package com.wolt.wdemo

object Config {
    const val BASE_URL = "https://restaurant-api.wolt.fi"
    const val SERVICE_COMMAND = "Command"
}