package com.wolt.wdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.wolt.wdemo.data.model.Result

internal class RestaurantsAdapter(
    private val onFavoriteClicked: (oid: String, isChecked: Boolean) -> Unit)
    : RecyclerView.Adapter<RestaurantsAdapter.RViewHolder>() {

    private var data: List<Result> = ArrayList() // internal

    class RViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val favBtn: AppCompatToggleButton = itemView.findViewById(R.id.row_rest_favourite_btn)
        private val nameTxtView: AppCompatTextView = itemView.findViewById(R.id.row_rest_name)
        private val descriptionTxtView: AppCompatTextView = itemView.findViewById(R.id.row_rest_description)
        private val imageView: AppCompatImageView = itemView.findViewById(R.id.row_rest_image_view)

        fun bind(result: Result) {

            result.name.whenNotNullNorEmpty {
                nameTxtView.text = result.name?.get(0)?.name
            }

            result.shortDescription.whenNotNullNorEmpty {
                descriptionTxtView.text = result.shortDescription?.get(0)?.name
            }

            favBtn.isChecked = result.favourite!!

            imageView.load(result.listImage) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_background)
                transformations(RoundedCornersTransformation(16f))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_restaurant, parent, false)
        return RViewHolder(view)
    }

    override fun onBindViewHolder(holder: RViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)

        holder.favBtn.isChecked = item.favourite!!
        holder.favBtn.setOnCheckedChangeListener { buttonView, isChecked ->
            if (holder.adapterPosition != RecyclerView.NO_POSITION && buttonView.isPressed) {
                data[position].favourite = isChecked
                val oid = item.activeMenu?.oid!!
                onFavoriteClicked.invoke(oid, isChecked)
            }
        }
    }

    fun refreshData(data: List<Result>) {
        val updating = this.data.count() != 0
        this.data = data

        if (updating)
            notifyItemRangeChanged(0, data.size - 1)
        else
            notifyDataSetChanged()
    }

    override fun getItemCount() = data.size
}