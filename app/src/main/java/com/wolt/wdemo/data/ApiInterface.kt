package com.wolt.wdemo.data

import com.wolt.wdemo.data.model.ResultData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("/v3/venues")
    suspend fun getRestaurants(@Query("lat") latitude: Double, @Query("lon") longitude: Double) : Response<ResultData>
}