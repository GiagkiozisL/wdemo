package com.wolt.wdemo.data

import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val service: ApiInterface) {

    suspend fun fetchRestaurants(lat: Double, lng: Double) = service.getRestaurants(lat, lng)
}