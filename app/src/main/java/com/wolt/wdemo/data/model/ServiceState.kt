package com.wolt.wdemo.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ServiceState : Parcelable {
    START,
    STOP
}