package com.wolt.wdemo.data

import android.content.Context
import com.wolt.wdemo.data.model.NetworkResult
import com.wolt.wdemo.data.model.ResultData
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


@ActivityRetainedScoped
class MainRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    @ApplicationContext context: Context
): ApiResponse(context) {

    suspend fun restaurants(lat: Double, lng: Double): Flow<NetworkResult<ResultData>>  {
        return flow {
            emit(safeApiCall { remoteDataSource.fetchRestaurants(lat, lng) })
        }.flowOn(Dispatchers.IO)
    }
}