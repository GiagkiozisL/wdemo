package com.wolt.wdemo.data.model

import com.squareup.moshi.Json

data class Location(
    @field:Json(name = "coordinates")
    val coordinates: List<Double>,

    @field:Json(name = "type")
    val type: String
)
