package com.wolt.wdemo.data

import android.content.Context
import com.wolt.wdemo.Utils.Companion.hasInternetConnection
import com.wolt.wdemo.data.model.NetworkResult
import retrofit2.Response

abstract class ApiResponse(private val context: Context) {

    suspend fun <T> safeApiCall(apiCall: suspend () -> Response<T>): NetworkResult<T> {
        if (hasInternetConnection(context)) {
            try {
                val response = apiCall()
                if (response.isSuccessful) {
                    val body = response.body()
                    body?.let {
                        return NetworkResult.Success(body)
                    }
                }
                return error("${response.code()} ${response.message()}")
            } catch (e: Exception) {
                return error(e.message ?: e.toString())
            }
        }
        return error("No internet connection !")
    }

    private fun <T> error(errorMessage: String): NetworkResult<T> =
        NetworkResult.Error("Network call failed : $errorMessage")
}