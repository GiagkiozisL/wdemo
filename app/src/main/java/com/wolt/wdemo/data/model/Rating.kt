package com.wolt.wdemo.data.model

import com.squareup.moshi.Json

data class Rating(
    @field:Json(name = "negative_percentage")
    val negativePercentage: Int? = 0,

    @field:Json(name = "neutral_percentage")
    val neutralPercentage: Int? = 0,

    @field:Json(name = "positive_percentage")
    val positivePercentage: Int? = 0,

    @field:Json(name = "rating")
    val rating: Int? = 0,

    @field:Json(name = "score")
    val score: Double? = 0.0,

    @field:Json(name = "text")
    val text: String? = "0",

    @field:Json(name = "volume")
    val volume: Int? = 0
)
