package com.wolt.wdemo.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Result(
    @field:Json(name = "active_menu")
    val activeMenu: ActiveMenu? = null,

    @field:Json(name = "address")
    val address: String,

    @field:Json(name = "alive")
    val alive: Long,

    /**
     *  ## developer message ##
     *
     *  bypassing some unnecessary (for this test) data structures
     *  was on purpose,
     *  to save valuable time for the test
     * */

    @field:Json(name = "always_available")
    val alwaysAvailable: Boolean? = null,

    @field:Json(name = "b2b_recommended")
    val b2BRecommended: Boolean? = null,

    @field:Json(name = "bank_account_type")
    val bankAccountType: String? = null,

    @field:Json(name = "bank_routing_code")
    val bankRoutingCode: String? = null,

    @field:Json(name = "cart_view_enabled")
    val cartViewEnabled: Boolean? = null,

    @field:Json(name = "city")
    val city: String? = null,

    @field:Json(name = "comment_disabled")
    val commentDisabled: Boolean? = null,

    @field:Json(name = "completion_estimates")
    val completionEstimates: CompletionEstimates? = null,

    @field:Json(name = "country")
    val country: String? = null,

    @field:Json(name = "currency")
    val currency: String? = null,

    @field:Json(name = "customer_support_phone")
    val customerSupportPhone: String? = null,

    @field:Json(name = "delivery_methods")
    val deliveryMethods: List<String>? = null,

    @field:Json(name = "dropoff_note_prefix")
    val dropoffNotePrefix: String? = null,

    @field:Json(name = "favourite")
    var favourite: Boolean? = null,

    @field:Json(name = "food_tags")
    val foodTags: List<String>? = null,

    @field:Json(name = "group_order_enabled")
    val groupOrderEnabled: Boolean? = null,

    @field:Json(name = "high_volume_venue")
    val highVolumeVenue: Boolean? = null,

    @field:Json(name = "id")
    val id: ActiveMenu? = null,

    @field:Json(name = "ipad_free")
    val ipadFree: Boolean? = null,

    @field:Json(name = "is_wolt_plus")
    val isWoltPlus: Boolean? = null,

    @field:Json(name = "itemid")
    val itemId: ActiveMenu? = null,

    @field:Json(name = "listimage")
    val listImage: String? = null,

    @field:Json(name = "listimage_blurhash")
    val listImageBlurhash: String? = null,

    @field:Json(name = "location")
    val location: Location? = null,

    @field:Json(name = "mainimage")
    val mainImage: String? = null,

    @field:Json(name = "mainimage_blurhash")
    val mainImageBlurhash: String? = null,

    @field:Json(name = "name")
    val name: List<Description>? = null,

    @field:Json(name = "online")
    val online: Boolean? = null,

    @field:Json(name = "phone")
    val phone: String? = null,

    @field:Json(name = "post_code")
    val postCode: String? = null,

    @field:Json(name = "preorder_enabled")
    val preorderEnabled: Boolean? = null,

    @field:Json(name = "preorder_only")
    val preorderOnly: Boolean? = null,

    @field:Json(name = "presence")
    val presence: String? = null,

    @field:Json(name = "price_range")
    val priceRange: Long? = null,

    @field:Json(name = "product_line")
    val productLine: String? = null,

    @field:Json(name = "public_url")
    val publicURL: String? = null,

    @field:Json(name = "public_visible")
    val publicVisible: Boolean? = null,

    @field:Json(name = "ratings_and_reviews_enabled")
    val ratingsAndReviewsEnabled: Boolean? = null,

    @field:Json(name = "relevancy")
    val relevancy: Double? = null,

    @field:Json(name = "relevancy_from_purchases")
    val relevancyFromPurchases: Double? = null,

    @field:Json(name = "short_description")
    val shortDescription: List<Description>? = null,

    @field:Json(name = "show_allergy_disclaimer_on_menu")
    val showAllergyDisclaimerOnMenu: Boolean? = null,

    @field:Json(name = "show_delivery_info_on_merchant")
    val showDeliveryInfoOnMerchant: Boolean? = null,

    @field:Json(name = "show_delivery_price_on_merchant")
    val showDeliveryPriceOnMerchant: Boolean? = null,

    @field:Json(name = "show_item_bottom_sheet")
    val showItemBottomSheet: Boolean? = null,

    @field:Json(name = "show_phone_number_on_merchant")
    val showPhoneNumberOnMerchant: Boolean? = null,

    @field:Json(name = "slug")
    val slug: String? = null,

    @field:Json(name = "status")
    val status: String? = null,

    @field:Json(name = "substitutions_enabled")
    val substitutionsEnabled: Boolean? = null,

    @field:Json(name = "timezone")
    val timezone: String? = null,

    @field:Json(name = "timezone_name")
    val timezoneName: String? = null,

    @field:Json(name = "type")
    val type: String? = null,

    @field:Json(name = "website")
    val website: String? = null,

    @field:Json(name = "wolt_delivery")
    val woltDelivery: Boolean? = null,

    @field:Json(name = "rating")
    val rating: Rating? = null
)
