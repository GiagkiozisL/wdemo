package com.wolt.wdemo.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ActiveMenu(
    @field:Json(name = "\$oid")
    val oid: String? = null
)
