package com.wolt.wdemo.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Description(
    @field:Json(name = "lang")
    val lang: String? = null,

    @field:Json(name = "value")
    val name: String? = null,
)
