package com.wolt.wdemo.data.model

data class AllowedPaymentMethod(val type: String)
