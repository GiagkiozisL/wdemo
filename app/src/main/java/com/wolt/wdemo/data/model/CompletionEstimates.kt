package com.wolt.wdemo.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CompletionEstimates(
    @field:Json(name = "delivery")
    val delivery: String? = null,

    @field:Json(name = "delivery_rush")
    val deliveryRush: String? = null,

    @field:Json(name = "normal")
    val normal: String? = null,

    @field:Json(name = "order_estimates_in_use")
    val orderEstimatesInUse: Boolean? = null,

    @field:Json(name = "rush")
    val rush: String? = null
)
