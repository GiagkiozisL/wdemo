package com.wolt.wdemo

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.wolt.wdemo.di.LocationData
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.wolt.wdemo", appContext.packageName)
    }


    /**
     *
     * testing LocationData rotation mechanism
     * to ensure for normal rotation of indexing
     * (Prevention OutOfBounds Exception)
     *
     * **/
    @DelicateCoroutinesApi
    @Test
    fun testLocDataRotation() {

        val dataLocation = LocationData()
        val nowMillis = System.currentTimeMillis()
        dataLocation.appOpenedMillis = nowMillis - 5000 // assuming app opened 5 secs before

        startCoroutineTimer(0, 5000) {
            val latLng = dataLocation.getCoordinates()
            val index = dataLocation.coordinates.indexOf(latLng)
            println("testLocDataRotation $index")
        }
    }

    @DelicateCoroutinesApi
    inline fun startCoroutineTimer(delayMillis: Long = 0, repeatMillis: Long = 0, crossinline action: () -> Unit) = GlobalScope.launch {
        delay(delayMillis)
        if (repeatMillis > 0) {
            while (true) {
                action()
                delay(repeatMillis)
            }
        } else {
            action()
        }
    }
}